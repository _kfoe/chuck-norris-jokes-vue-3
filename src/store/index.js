import {
  createStore
} from 'vuex'
import Axios from 'axios'

export default createStore({
  state: {
    joke: []
  },
  mutations: {
  	SET_JOKE: (state, joke) => state.joke = joke 
  },
  actions: {
    fetchJoke: ({
      commit,
      state
    }) => Axios.get('https://api.chucknorris.io/jokes/random').then((r) => commit('SET_JOKE', r.data))
  },
  getters: {
    joke: (state) => state.joke
  }
})